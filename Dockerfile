FROM debian:bullseye-slim

LABEL org.opencontainers.image.source="https://gitlab.com/timxzl/slurm-docker-cluster.git" \
      org.opencontainers.image.title="slurm-docker-cluster" \
      org.opencontainers.image.description="Slurm Docker cluster" \
      org.label-schema.docker.cmd="docker-compose up -d" \
      maintainer="timxzl"

ARG SLURM_TAG=slurm-21-08-8-2
ARG SLURM_UID=3000
ARG SLURM_GID=3000
ARG MUNGE_UID=115
ARG MUNGE_GID=119
ARG GOSU_VERSION=1.14

RUN set -eux; \
    groupadd -r --gid=$MUNGE_GID munge \
    && useradd -r -g munge --uid=$MUNGE_UID munge \
    && mkdir -p /run/munge && chown munge:munge /run/munge \
    && groupadd -r --gid=$SLURM_GID slurm \
    && useradd -r -g slurm --uid=$SLURM_UID slurm

RUN set -eux; \
    savedAptMark="$(apt-mark showmanual)"; \
    apt-get update; \
    apt-get install -y --no-install-recommends ca-certificates wget gnupg2; \
    dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
    wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
    export GNUPGHOME="$(mktemp -d)"; \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
    gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
    gpgconf --kill all; \
    rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual ca-certificates > /dev/null; \
    [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark > /dev/null; \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    chmod +x /usr/local/bin/gosu; \
    gosu --version; \
    gosu nobody true

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
       gcc \
       g++\
       git \
       make \
       munge \
       python3 \
       libmunge-dev \
       libmariadb-dev \
       mariadb-client-core-10.5; \
    rm /etc/munge/munge.key
#       bzip2 \
#       perl \
#       gnupg \
#       python-devel \
#       python-pip \
#       python34 \
#       python34-devel \
#       python34-pip \
#       mariadb-server \
#       mariadb-devel \
#       psmisc \
#       bash-completion \
#       vim-enhanced \
#    && yum clean all \
#    && rm -rf /var/cache/yum
    
#RUN ln -s /usr/bin/python3.4 /usr/bin/python3

#RUN pip install Cython nose && pip3.4 install Cython nose

RUN set -x \
    && git clone https://github.com/SchedMD/slurm.git \
    && cd slurm \
    && git checkout tags/$SLURM_TAG \
    && ./configure --prefix=/opt/slurm --sysconfdir=/opt/slurm/etc --with-munge=yes \
        --with-mysql_config=/usr/bin \
    && make -j 4 && make install \
    && install -D -m644 contribs/slurm_completion_help/slurm_completion.sh /etc/profile.d/slurm_completion.sh \
    && cd .. \
    && rm -rf slurm \
    && mkdir /opt/slurm/etc \
        /var/spool/slurm \
        /var/run/slurm \
        /var/log/slurm \
    && chown -R slurm:slurm /var/*/slurm*
    #&& install -D -m644 etc/cgroup.conf.example /etc/slurm/cgroup.conf.example \
    #&& install -D -m644 etc/slurm.conf.example /etc/slurm/slurm.conf.example \
    #&& install -D -m644 etc/slurmdbd.conf.example /etc/slurm/slurmdbd.conf.example \
        #/var/lib/slurmd \
        #/data \
#    && touch /var/lib/slurmd/node_state \
#        /var/lib/slurmd/front_end_state \
#        /var/lib/slurmd/job_state \
#        /var/lib/slurmd/resv_state \
#        /var/lib/slurmd/trigger_state \
#        /var/lib/slurmd/assoc_mgr_state \
#        /var/lib/slurmd/assoc_usage \
#        /var/lib/slurmd/qos_usage \
#        /var/lib/slurmd/fed_mgr_state \
#    && /sbin/create-munge-key

#COPY slurm.conf /etc/slurm/slurm.conf
#COPY slurmdbd.conf /etc/slurm/slurmdbd.conf

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["slurmdbd"]
